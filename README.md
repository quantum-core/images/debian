# Debian 10
This image is used to compile the core for debian and is based on ``debian:10``.
It is used as an intermediate container for building the final QuantumCore docker image.

Also this image is used for automated tests.

## Used software
| Name        | Version |
|-------------|---------|
| clang       | 11      |
| cmake       | 3.13.4  |
| Boost       | 1.72.0  |
| AngelScript | 2.34.0  |