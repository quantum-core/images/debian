FROM debian:10
LABEL os="debian"

# Fix issue with apt in docker
COPY ./badproxy /etc/apt/apt.conf.d/99fixbadproxy

# Install wget
RUN apt-get update && apt-get install --no-install-recommends -y wget ca-certificates gnupg2 && rm -rf /var/lib/apt/lists/*

# Add LLVM repository
RUN echo 'deb http://apt.llvm.org/buster/ llvm-toolchain-buster main' >> /etc/apt/sources.list
RUN echo 'deb-src http://apt.llvm.org/buster/ llvm-toolchain-buster main' >> /etc/apt/sources.list
RUN wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -

# Install build tools
RUN apt-get update && apt-get install --no-install-recommends -y build-essential git curl cmake unzip clang-11 libc++-11-dev libc++abi-11-dev libc++1-11 libc++abi1-11 lld-11 lldb-11 llvm-11 llvm-11-dev libclang-11-dev libclang1-11  && rm -rf /var/lib/apt/lists/*

# Enable llvm as compiler
RUN update-alternatives --install /usr/bin/cc cc /usr/bin/clang-11 100
RUN update-alternatives --install /usr/bin/clang clang /usr/bin/clang-11 100
RUN update-alternatives --install /usr/bin/c++ c++ /usr/bin/clang++-11 100
RUN update-alternatives --install /usr/bin/g++ g++ /usr/bin/clang++-11 100
RUN update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-11 100

######################
## General ###########
######################

# Settings environment variables
ENV CC=/usr/bin/clang-11
ENV CXX=/usr/bin/clang++-11

######################
## VCPKG #############
######################

# Clone vcpkg
RUN git clone https://github.com/microsoft/vcpkg.git /opt/vcpkg

# Bootstrap vcpkg
RUN cd /opt/vcpkg && ./bootstrap-vcpkg.sh

# Apply libmariadb patch to fix current issue #10024
# RUN cd /opt/vcpkg && wget https://patch-diff.githubusercontent.com/raw/microsoft/vcpkg/pull/10043.patch && git apply 10043.patch

# Install QuantumCore required packages
RUN cd /opt/vcpkg && ./vcpkg install boost-convert:x64-linux boost-log:x64-linux boost-system:x64-linux boost-filesystem:x64-linux boost-program-options:x64-linux libmariadb:x64-linux angelscript:x64-linux gtest:x64-linux cryptopp:x64-linux
